package be.kdg.prog1.ecomproject;

import java.util.Objects;

public class FiltersAndFiltrationSystem extends IndustrialAndScienceProduct implements Taxable{
    private String Material;
    private boolean airFilter;
    private boolean waterFilter;
    private double throughput;// /per min


    public FiltersAndFiltrationSystem(String environment, boolean humanUse, boolean reUsable,
                                      float price, String name, String description, String material, boolean airFilter,
                                      boolean waterFilter, double throughput) {
        super(environment, humanUse, reUsable,  price, name, description);
        this.Material = material;
        this.airFilter = airFilter;
        this.waterFilter = waterFilter;
        this.throughput = throughput;
    }

    public FiltersAndFiltrationSystem(String environment, boolean humanUse, boolean reUsable, float price, String name,
                                      String description, String Material, boolean airFilter, boolean waterFilter) {
        super( environment, humanUse, reUsable, price, name, description);
        this.Material = Material;
        this.airFilter = airFilter;
        this.waterFilter = waterFilter;
        this.throughput = 1;
    }

    public String getMaterial() {
        return Material;
    }

    public boolean isAirFilter() {
        return airFilter;
    }

    public boolean isWaterFilter() {
        return waterFilter;
    }

    public double getThroughput() {
        return throughput;
    }

    public void setMaterial(String material) {
        Material = material;
    }

    public void setAirFilter(boolean airFilter) {
        this.airFilter = airFilter;
    }

    public void setWaterFilter(boolean waterFilter) {
        this.waterFilter = waterFilter;
    }

    public void setThroughput(double throughput) {
        this.throughput = throughput;
    }

    @Override
    public String toString() {
        String FiltersAndFiltrationSystem = "Material= %s, airFilter= %b, waterFilter= %b, throughput= %d";
        return super.toString() + String.format(FiltersAndFiltrationSystem, Material, airFilter, waterFilter, throughput);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FiltersAndFiltrationSystem)) return false;
        if (!super.equals(o)) return false;
        FiltersAndFiltrationSystem that = (FiltersAndFiltrationSystem) o;
        return isAirFilter() == that.isAirFilter() && isWaterFilter() == that.isWaterFilter() && Double.compare(that.getThroughput(), getThroughput()) == 0 && getMaterial().equals(that.getMaterial());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getMaterial(), isAirFilter(), isWaterFilter(), getThroughput());
    }
}
