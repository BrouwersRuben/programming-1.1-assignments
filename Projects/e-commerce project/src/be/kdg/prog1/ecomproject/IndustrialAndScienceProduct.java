package be.kdg.prog1.ecomproject;

import java.util.Objects;

public class IndustrialAndScienceProduct implements Taxable{

    protected boolean isElectric;
    protected useCase usecase;

    protected long SKU;
    protected float Price;
    protected String Name;
    protected String Description;
    protected final static double VAT = 1.21;

    public IndustrialAndScienceProduct(boolean isElectric, String environment, boolean humanUse, boolean reUsable,
                                       float price, String name, String description) {
        this.isElectric = isElectric;
        this.usecase = new useCase(environment, humanUse, reUsable);
        this.SKU = hashCode();
        Price = price;
        Name = name;
        Description = description;
    }

    public IndustrialAndScienceProduct(String environment, boolean humanUse, boolean reUsable, float price, String name,
                                       String description) {
        this.isElectric = true;
        this.usecase = new useCase(environment, humanUse, reUsable);
        this.SKU = hashCode();
        this.Price = price;
        this.Name = name;
        this.Description = description;
    }

    public boolean isElectric() {
        return isElectric;
    }

    public useCase getUsecase() {
        return usecase;
    }

    public long getSKU() {
        return SKU;
    }

    public String getName() {
        return Name;
    }

    public String getDescription() {
        return Description;
    }

    public double getVatPercentage() {
        return VAT;
    }

    public double getTax(){
        return (this.Price*(this.VAT-1));
    }

    public double getPriceIncludingVat(){
        return (this.Price+getTax());
    }

    public double getPriceExcludingVat(){
        return this.Price;
    }

    public void setElectric(boolean electric) {
        isElectric = electric;
    }

    public void setUsecase(useCase usecase) {
        this.usecase = usecase;
    }

    public void setSKU(long SKU) {
        this.SKU = SKU;
    }

    public void setPrice(float price) {
        Price = price;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public String toString() {
        String IndusScienProd = "isElectric= %b, %s, SKU= %d, Price= %d, Name= %s, Description= %s";
        return String.format(IndusScienProd, isElectric, usecase.toString(), SKU, Price, Name, Description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IndustrialAndScienceProduct)) return false;
        IndustrialAndScienceProduct that = (IndustrialAndScienceProduct) o;
        return isElectric() == that.isElectric() && getSKU() == that.getSKU() && Float.compare(that.Price, Price) == 0 && Objects.equals(getUsecase(),
                that.getUsecase()) && Objects.equals(getName(), that.getName()) && Objects.equals(getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(isElectric(), getUsecase(), getSKU(), getPriceExcludingVat(), getName(), getDescription());
    }
}
