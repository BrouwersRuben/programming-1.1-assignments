package be.kdg.prog1.ecomproject;

import java.util.Objects;

class useCase {
    private final String environment;
    private final boolean humanUse;
    private final boolean reUsable;

    public useCase(String environment, boolean humanUse, boolean reUsable) {
        this.environment = environment;
        this.humanUse = humanUse;
        this.reUsable = reUsable;
    }

    public String getEnvironment() {
        return environment;
    }

    public boolean isHumanUse() {
        return humanUse;
    }

    public boolean isReUsable() {
        return reUsable;
    }

    public String toString() {
        String useCase = "Environment= %s, humanUse= %b, reUsable= %b";
        return String.format(useCase, environment, humanUse, reUsable);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof useCase)) return false;
        useCase useCase = (useCase) o;
        return isHumanUse() == useCase.isHumanUse() && isReUsable() == useCase.isReUsable() && getEnvironment().equals(useCase.getEnvironment());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEnvironment(), isHumanUse(), isReUsable());
    }
}
