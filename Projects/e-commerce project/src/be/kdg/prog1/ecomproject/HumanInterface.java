package be.kdg.prog1.ecomproject;

import java.util.Scanner;

public class HumanInterface {

    public static void Logo() {
        System.out.println(" _____                     _ _   _     _        _       _                      \n" +
                "|  __ \\                   | | | | |   | |      (_)     | |                     \n" +
                "| |  \\/_ __ __ _ _ __   __| | | | |___| |_ _ __ _  __ _| |  ___ ___  _ __ ___  \n" +
                "| | __| '__/ _` | '_ \\ / _` | | | / __| __| '__| |/ _` | | / __/ _ \\| '_ ` _ \\ \n" +
                "| |_\\ \\ | | (_| | | | | (_| | |_| \\__ \\ |_| |  | | (_| | || (_| (_) | | | | | |\n" +
                " \\____/_|  \\__,_|_| |_|\\__,_|\\___/|___/\\__|_|  |_|\\__,_|_(_)___\\___/|_| |_| |_|");
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
    }

    public static String Name() {
        final int whitelines = 20;
        final int restTime = 5;

        String costumerName;
        Scanner scanner = new Scanner(System.in);

        Logo();

        System.out.print("What is your full name? ");
        costumerName = scanner.nextLine();

        System.out.println();

        System.out.printf("\t\t\t\tWelcome to GrandUstrial.com \"%s\"\n", costumerName);
        System.out.println("\t\tYou will be forwarded to our available products in a few seconds");

        try {
//This gives the user time to read the output of the text above
            Thread.sleep(restTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//Adds some lines between this screen and the next, so that it looks a bit more like a real 'website'
        for (int i = 0; i < whitelines; i++) {
            System.out.println();
        }
        return costumerName;
    }

    public static void ProductList(IndustrialAndScienceProduct[] product_array) {

        int ProductNumber = 1;

        System.out.println(' ');
        System.out.println("Industrial and Science products");
        System.out.println("-------------------------------------------------------------------------------");
        for (IndustrialAndScienceProduct products : product_array) {
                System.out.printf("%d | %s\n", ProductNumber++, products.getName());
                System.out.printf("\t DESCRIPTION: %s \n", products.getDescription());
                System.out.printf("\t PRICE: €%.2f\n", products.getPriceExcludingVat());
            }
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println();
    }

    public static Sale Input(Costumer costumer, IndustrialAndScienceProduct[] product_array){
        Scanner scanner = new Scanner(System.in);

        Sale purchase = new Sale(costumer);

        final int whitelines = 30;
        final int amountOfStock = 1000;
        int ChooseNumber;
        boolean FaultyinputC;
        int ChooseQuantity;
        boolean FaultyinputQ;

        do {
            Logo();
            ProductList(product_array);

            do {
                System.out.print("Please enter the number of the project you would like to buy (0=stop): ");
                ChooseNumber = scanner.nextInt();
                if (ChooseNumber > product_array.length || ChooseNumber < 0) {
                    System.out.println("This is not a valid number, please try again");
                    FaultyinputC = true;
                    scanner.nextLine();
                }
            } while (FaultyinputC = false);
            if (ChooseNumber != 0) {
                do {
                    System.out.print("How many of these would you like: ");
                    ChooseQuantity = scanner.nextInt();
                    FaultyinputQ = false;
                    if (ChooseQuantity > amountOfStock || ChooseQuantity <= 0) {
                        System.out.println("enter a quantity between 0 and 1000 please");
                        FaultyinputQ = true;
                        scanner.nextLine();
                    }
                } while (FaultyinputQ);
                for (int i = 0; i < purchase.getExistingSales().length; i++) {
                    if(purchase.SalesAlreadyInTable(i) == null) {
                        purchase.PutSalesInTable(product_array[ChooseNumber-1], ChooseQuantity, i);
                        break;
                    } else if (purchase.SalesAlreadyInTable(i).getProduct().equals(product_array[ChooseNumber-1])) {
                        purchase.SalesAlreadyInTable(i).incrementChosenQuantity(ChooseQuantity);
                        break;
                    }
                }
            }
            for (int i = 0; i < whitelines; i++){
                System.out.println();}
        }while (ChooseNumber != 0);
        return purchase;
    }
}


