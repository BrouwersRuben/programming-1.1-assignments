package M1.Solutions.A11_sequence.src.be.kdg;

import java.util.Scanner;

public class Sequence {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Input:
        System.out.print("How many numbers do you want to print? ");
        int number = scanner.nextInt();
        System.out.print("What is the starting value? ");
        int base = scanner.nextInt();
        System.out.print("What is the increment? ");
        int increment = scanner.nextInt();

        //Loop en process:
        int counter = 0;
        while(counter < number) {
            System.out.print(base + " ");
            base = base + increment;
            counter++;
        }
    }
}
