package M1.Solutions.A10_summation.src.be.kdg;

import java.util.Scanner;

public class Summation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sum = 0; //important: initialisation!
        int number;

        while(true) {
            System.out.print("Enter a number (to stop enter 0): ");
            number = scanner.nextInt();
            sum = sum + number; //OR: sum += number;
            if(number == 0) {
                System.out.println("The sum of these numbers is: " + sum);
                return;
            }
        }
    }
}
