package M4.A08Substring;

import java.util.Scanner;

public class Substring {
    /*
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        StringBuilder sb1 = new StringBuilder();

        int count = 0;
        String charComb;
        String sentence;

        System.out.print("Please enter your sentence: ");
        sentence = sc.nextLine();
        System.out.print("Please enter what occurrence you would like to count: ");
        charComb = sc.nextLine();


        sb1.append(sentence);
        String[] sentenceArray = sb1.toString().split(" ");

        for (String word : sentenceArray) {
            if (word.equals(charComb)) {
                count++;
            }
        }

        System.out.printf("There are %d occurrences of \"%s\" in the sentence \"%s\"", count, counter, sentence);
    }

     */
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String line;
        int counter = 0;

        System.out.println("Enter a line of text");
        line = s.nextLine();
        StringBuilder sb1 = new StringBuilder(line);

        for (int i = 0; i < line.length(); i++) {
            if (sb1.charAt(i) == 'o') {
                if (sb1.charAt(i + 1) == 'u') {
                    counter++;
                }
            }
        }
        System.out.println("Substring \"ou\" appears " + counter + " time(s).");
    }
}

