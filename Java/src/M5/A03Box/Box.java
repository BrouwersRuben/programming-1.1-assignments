package M5.A03Box;

public class Box {
    String type;
    double length;
    double width;
    double height;

    public Box(String type, double length, double width, double height) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public Box(String type, double length) {
        this.type = type;
        this.length = length;
        this.width = length;
        this.height = length;
    }

    public double surface() {
        return 2 * (height * width + height * length + width * length);
    }

    public double volume() {
        return width * height * length;
    }

    public double tapeLenght() {
        return (2 *(width+height));
    }

    public String toString() {
        return "Type: " + type + "\n" +
                "Length: " + length + " cm\n" +
                "Width: " + width + " cm\n" +
                "Height: " + height + " cm\n" +
                "Surface: " + surface() + " cm²\n" +
                "Volume: " + volume() + " cm³ \n" +
                "Minumum tapelength: " + tapeLenght() + " cm \n";
    }
}