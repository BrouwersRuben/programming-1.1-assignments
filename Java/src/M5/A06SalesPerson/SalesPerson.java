package M5.A06SalesPerson;

public class SalesPerson {
    String name;
    double revenue;

    public SalesPerson(String name, double revenue) {
        this.name = name;
        this.revenue = revenue;
    }

    public String getName() {
        return name;
    }

    public double getRevenue() {
        return revenue;
    }
    public boolean hasMoreRevenueThan (SalesPerson other) {
        return revenue > other.getRevenue();
    }
}
