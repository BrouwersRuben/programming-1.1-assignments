package M5.Testing.A06SalesPerson;

public class SalesPerson {
    private String name;
    private double revenue;

    public SalesPerson(String name, double revenue) {
        this.name = name;
        this.revenue = revenue;
    }

    public String getName() {
        return name;
    }

    public double getRevenue() {
        return revenue;
    }

    public boolean hasMoreRevenueThen(SalesPerson other){
        return (this.getRevenue() > other.getRevenue() ? true : false);
    }

    public static SalesPerson topEarner(SalesPerson one, SalesPerson two, SalesPerson three) {
        if (one.hasMoreRevenueThen(two) && one.hasMoreRevenueThen(three)){
            return one;
        } else if (two.hasMoreRevenueThen(one) && two.hasMoreRevenueThen(three)){
            return two;
        } else {
            return three;
        }
    }
}
