package M5.Testing.A02BankAccount;

public class BankAccount {
    private String holder;
    private String iban;
    private double balance;

    public BankAccount(String holder, String iban, double balance) {
        this.holder = holder;
        this.iban = iban;
        this.balance = balance;
    }

    public BankAccount(String holder, String iban) {
        this(holder, iban, 0);
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public boolean withdraw(double amount) {
        if (balance == 0 || (balance-amount) < 0) {
            return false;
        } else {
            balance -= amount;
            return true;
        }
    }

    @Override
    public String toString() {
        return String.format("The account %s of %s has a balance of %.2f", iban
                ,holder,
                balance);
    }
}
