package M5.Testing.A04Operations;

public class Operations {
    private double numberOne;
    private double numberTwo;

    public Operations(double numberOne, double numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    public double sum(){
        return numberOne + numberTwo;
    }

    public double difference(){
        return numberOne - numberTwo;
    }

    public double product(){
        return numberOne * numberTwo;
    }

    public double quotient(){
        return numberOne/numberTwo;
    }
}
