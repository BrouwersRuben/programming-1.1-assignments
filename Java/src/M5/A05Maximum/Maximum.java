package M5.A05Maximum;

public class Maximum {
    int one;
    int two;
    int three;

    public Maximum() {}

    public Maximum(int one, int two, int three) {
        this.one = one;
        this.two = two;
        this.three = three;
    }

    public Maximum(long one, long two, long three) {
        this.one = (int) one;
        this.two = (int) two;
        this.three = (int) three;
    }

    public Maximum(double one, double two, double three) {
        this.one = (int) one;
        this.two = (int) two;
        this.three = (int) three;
    }

    public double max(){
        return Math.max(Math.max(one,two), three);
    }

    public double max(int one, int two, int three){
        return Math.max(Math.max(one,two), three);
    }

    public double max(long one, long two, long three){
        return Math.max(Math.max(one,two), three);
    }

    public double max(double one, double two, double three){
        return Math.max(Math.max(one,two), three);
    }


}
