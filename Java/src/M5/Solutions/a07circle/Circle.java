package M5.Solutions.a07circle;

/**
 * @author Kristiaan Behiels
 * @version 1.0 12/10/2014
 *
 */
public class Circle {
    private int radius;
    private String colour = "Zwart";

    public Circle(int radius, String colour) {
        this.radius = radius;
        this.colour = colour;
    }

    public Circle(int radius) {
        this.radius = radius;
    }

    //  2 x PI x r
    public double circumference() {
        return 2 * Math.PI * radius;
    }

    //
    // PI * r²
    public double surface() {
        return Math.PI * radius * radius;
    }

    // Absolute difference circumference
    public double deltaCircumference(Circle other) {
       return Math.abs(this.circumference() - other.circumference());
    }

	// Absolute difference surface
    public double deltaSurface(Circle other) {
        return Math.abs(this.surface() - other.surface());
    }

    @Override
    public String toString() {
        return String.format("Colour: %s%nRadius: %d%nCircumference: %.2f%nSurface: %.2f%n",
	        colour, radius, circumference(), surface());
    }
}
