package M7.A13Student;

public class Phone{
    protected String number;

    public Phone(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("Phone number: %s, ", number);
    }
}
