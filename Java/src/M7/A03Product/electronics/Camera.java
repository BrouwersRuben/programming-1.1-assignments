package M7.A03Product.electronics;

import M7.A03Product.Product;

public class Camera extends Product {
        private int pixels;

        public int getPixels() {
            return pixels;
        }

    public Camera(int pixels, String code, String description, float price) {
        super(code, description, price);
        this.pixels = pixels;
    }
    public double getVat(){
        return price*0.21;
    }

    @Override
    public String toString() {
        return String.format("Pixels: %d, Price %.2f, Code: %s, Description: %s%n", getPixels(), getPrice(), getCode(), getDescription() );
    }
}
