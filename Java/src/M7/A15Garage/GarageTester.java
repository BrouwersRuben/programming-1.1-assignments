package M7.A15Garage;

public class GarageTester {
    public static void main(String[] args) {
        Car car = new Car("Ford S-MAX");
        Garage garage = new Garage("Neyt");
        car.setGarage(garage);

        System.out.println(car);  // implicit call to toString

        car = new Car(garage, "Ford Focus");

        System.out.println(car);  // implicit call to toString

        car = new Car(new Garage("Van Winkel"), "Mercedes C");

        System.out.println(car);  // impliciete call to toString

        Car copy = new Car(car);

        System.out.println(copy);  // implicit call to toString
    }
}
