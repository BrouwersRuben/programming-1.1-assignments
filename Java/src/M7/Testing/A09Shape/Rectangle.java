package M7.Testing.A09Shape;

public class Rectangle extends Shape{
    int height;
    int width;

    public Rectangle(int x, int y) {
        super(x, y);
    }

    public Rectangle() {}

    public Rectangle(int x, int y, int height, int width) {
        super(x, y);
        this.height = height;
        this.width = width;
    }

    public double getArea() {
        return width*height;
    }

    public double getPerimeter(){
        return 2*(width+height);
    }
}
