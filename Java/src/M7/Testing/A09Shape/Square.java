package M7.Testing.A09Shape;

public class Square extends Rectangle{
    int size;

    public Square(int size) {
        this.size = size;
    }

    public Square(){}

    public Square(int x, int y) {
        super(x, y);
    }

    public Square(int x, int y, int size) {
        super(x, y);
        this.size = size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public double getArea() {
        return (int) (Math.pow(size, 2));
    }

    public double getPerimeter(){
        return 4*size;
    }
}
