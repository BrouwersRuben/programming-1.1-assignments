package M7.Testing.A09Shape;

public abstract class Shape {
    protected int x;
    protected int y;

    public abstract double getArea();
    public abstract double getPerimeter();

    public Shape(){}

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("%s at (%d,%d) with perimeter %.2f and surface %.2f", getClass().getSimpleName(), x, y, getPerimeter(), getArea());
    }
}
