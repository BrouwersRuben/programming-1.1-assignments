package M7.Testing.A07Dog.Animals;

public class Dog extends Animal {

    public Dog(String name, String breed, String colour, String chipNumber) {
        super(name, breed, colour, chipNumber, "Like a dog in a bowling game");
    }
}
