package M7.Testing.A07Dog.Animals;

public class Rabbit extends Animal{

    private boolean digs;

    public Rabbit(String name, String breed, String colour, boolean digs, String chipNumber) {
        super(name, breed, colour, chipNumber, "I'm an ice rabbit");
        this.digs = digs;
    }

}
