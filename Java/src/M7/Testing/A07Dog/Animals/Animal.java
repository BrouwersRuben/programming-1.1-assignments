package M7.Testing.A07Dog.Animals;

public class Animal {
    protected final String chipNumber;
    protected String name;
    protected String breed;
    protected String colour;
    protected String tagLine;

    public Animal(String name, String breed, String colour, String chipNumber, String tagLine) {
        this.name = name;
        this.breed = breed;
        this.colour = colour;
        this.chipNumber = chipNumber;
        this.tagLine = tagLine;
    }

    public String getChipNumber() {
        return chipNumber;
    }

    public String getTagLine() {
        return tagLine;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }
}
