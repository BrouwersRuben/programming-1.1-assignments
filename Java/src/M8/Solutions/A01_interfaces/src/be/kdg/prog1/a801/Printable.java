package M8.Solutions.A01_interfaces.src.be.kdg.prog1.a801;

public interface Printable {
    void print();
}
