package M8.A02Comparable;

public interface Comparable {
    boolean isEqualTo(Object o);
    boolean isGreaterThan(Object o);
    boolean isLessThan(Object o);
}
