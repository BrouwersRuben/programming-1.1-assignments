package M8.A01Interfaces;

public class Circle extends Shape {
    private int radius;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Circle( int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public void print() {
        System.out.println(getClass().getSimpleName() + "\n======");
        System.out.printf("Position: (%d, %d)\nRadius:   %d", getX(), getY(), radius );
        System.out.println();
    }
}
