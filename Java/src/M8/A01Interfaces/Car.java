package M8.A01Interfaces;

public class Car implements Printable {
    private String brand;
    private String model;
    private String licensePlate;

    public Car(String brand, String model, String licensePlate) {
        this.brand = brand;
        this.model = model;
        this.licensePlate = licensePlate;
    }

    @Override
    public void print() {
        System.out.println(getClass().getSimpleName() + "\n===");
        System.out.println("Brand:               " + brand);
        System.out.println("Model:               " +  model);
        System.out.println("License plate:       " + licensePlate);
        System.out.println();
    }
}

