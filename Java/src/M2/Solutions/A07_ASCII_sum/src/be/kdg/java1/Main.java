package M2.Solutions.A07_ASCII_sum.src.be.kdg.java1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string of text: ");
        String text = keyboard.nextLine();
        int sum = 0;
        for (char c : text.toCharArray()) {
            sum += c;
        }
        System.out.println("sum = " + sum);
    }
}
