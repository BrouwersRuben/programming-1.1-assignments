package M2.Solutions.A09_pizza.src.be.kdg.java1;

import java.util.Scanner;

public class Main {
    /*
    How many pizzas would you like: 3
    How many extra toppings for pizza 1: 2
    How many extra toppings for pizza 2: 3
    How many extra toppings for pizza 3: 0
    Total price: €26.5
     */
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final int PRICE_PIZZA = 800;
        final int PRICE_TOPPINGS = 50;
        int numberOfPizzas;
        int numberOfToppings;
        int count = 1;
        int totalPrice = 0;

        System.out.print("How many pizzas would you like: ");
        numberOfPizzas = keyboard.nextInt();
        totalPrice += numberOfPizzas * PRICE_PIZZA;
        while (count <= numberOfPizzas) {
            System.out.print("How many extra toppings for pizza " + count + ": ");
            numberOfToppings = keyboard.nextInt();
            totalPrice += numberOfToppings * PRICE_TOPPINGS;
            count++;
        }
        System.out.printf("Total price: €%.1f\n", totalPrice / 100.0);
    }
}
