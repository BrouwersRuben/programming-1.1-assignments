package M2.A09Pizza;

import java.util.Scanner;

public class Pizza {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        final int pizzaPrice = 800;
        int toppingPrice = 50;
        int pizzaAmount;
        int toppingAmount;
        int totalPrice;
        int i;

        System.out.print("How many pizza's would you like? ");
        pizzaAmount = keyboard.nextInt();
        totalPrice = pizzaPrice * pizzaAmount;

        for ( i = 1; i <= pizzaAmount; i++) {
            System.out.print("How many extra toppings for pizza" + i + "?: ");
            toppingAmount = keyboard.nextInt();

            if (toppingAmount != 0){
                totalPrice += (toppingAmount*toppingPrice);
            }

        }
        System.out.println("Your total amount is: €" + (double)totalPrice/100);

    }
}
