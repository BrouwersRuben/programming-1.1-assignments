package M2.A16ScrambleName;

import java.util.Random;
import java.util.Scanner;

public class ScrambleName {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();

        String name, nameCopy, nameScramble =" ";

        System.out.print("Enter your name: ");
        name = sc.nextLine();
        nameCopy = name;

        while (!nameCopy.isEmpty()){
            int position = rd.nextInt(nameCopy.length());
            char c = nameCopy.charAt(position);
            if (c != ' ') {
                nameScramble += c;
            }
            nameCopy = nameCopy.substring(0, position) + nameCopy.substring(position + 1);
        }
        System.out.printf("Hi %s, your name scrambled is %s", name, nameScramble);

    }
}
