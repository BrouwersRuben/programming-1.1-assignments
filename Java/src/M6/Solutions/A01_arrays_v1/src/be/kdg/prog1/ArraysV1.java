package M6.Solutions.A01_arrays_v1.src.be.kdg.prog1;

public class ArraysV1 {
    public static void main(String[] args) {
        // Part 1: declaring the arrays

        int[] numbers = new int[5];

        // double[] is fine as well since a double is a 'double precision floating point number'.
        float[] stockMarketRates = new float[20];

        boolean[] switches = new boolean[8];

        String[] words = new String[4];

        // Part 2: print the first element of each array
        System.out.println(numbers[0]);
        System.out.println(stockMarketRates[0]);
        System.out.println(switches[0]);
        System.out.println(words[0]);

        // Part 3: print the last element of each array
        System.out.println(numbers[4]); // or numbers[number.length - 1]
        System.out.println(stockMarketRates[19]); // or stockMarketRates[stockMarketRates.length - 1]
        System.out.println(switches[7]); // or switches[switches.length - 1]
        System.out.println(words[3]); // or words[words.length - 1]
    }
}
