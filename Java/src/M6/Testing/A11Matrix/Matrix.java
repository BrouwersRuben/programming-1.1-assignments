package M6.Testing.A11Matrix;

public class Matrix {
    public static void main(String[] args) {
        int[][] matrix = new int[4][6];

        for (int row = 0; row < matrix.length; row++){
            for (int col = 0; col < matrix[row].length; col++){
                matrix[row][col] = (col+1) * (row+1);
            }
        }

        for (int[] row: matrix) {
            for(double value : row) {
                System.out.printf(" %3.0f ", value);
            }
            System.out.println();
        }
    }
}
