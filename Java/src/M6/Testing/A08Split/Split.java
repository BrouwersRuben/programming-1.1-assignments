package M6.Testing.A08Split;

public class Split {
    public static void main(String[] args) {
        String text = "Java can be tricky at times";
        String[] string = text.split(" ");

        for (String words :  string){
            System.out.printf(" \"%s\"", words);
        }
    }
}
