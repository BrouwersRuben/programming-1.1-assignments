package M6.Testing.A05Temperatures;

import java.util.Random;
import java.util.Scanner;

public class Temperatures {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();
        int[] temperatures = new int[7];
        double averageTemp = 0;

        System.out.println("Please enter 7 temperatures: ");
        for (int day = 1; day < temperatures.length+1; day++){
            System.out.printf("Day %d: ", day);
            temperatures[day-1] = sc.nextInt();
        }

        System.out.println();

        System.out.println("Summary");
        System.out.println("==================");
        for (int day = 1; day < temperatures.length+1; day++){
            System.out.printf("Day %d : %10d\n", day, temperatures[day-1]);
        }
        System.out.println("==================");

        for (int day = 1; day < temperatures.length+1; day++){
            averageTemp += temperatures[day-1];
        }

        averageTemp /= temperatures.length;

        System.out.printf("Average: %.2f", averageTemp);
    }
}
