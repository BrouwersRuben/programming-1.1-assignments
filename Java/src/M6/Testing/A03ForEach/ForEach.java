package M6.Testing.A03ForEach;

public class ForEach {
    public static void main(String[] args) {
        int[] lotteryNumbers = {3, 13, 17, 31, 32, 43};
        int[] multiples = new int[20];

        for (int number: lotteryNumbers){
            System.out.printf("%d ",number);
        }
        System.out.println();

        for (int i = 0; i < multiples.length; i++){
            multiples[i] = (7*i)+7;
        }

        for(int multiple : multiples){
            System.out.printf("%d ", multiple);
        }

        System.out.println();

        for(int i = multiples.length-1; i >= 0; i--){
            System.out.print(multiples[i] + " ");
        }




    }
}
