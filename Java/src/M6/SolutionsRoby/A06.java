package M6.SolutionsRoby;

import java.util.Scanner;
import java.util.Locale;

public class A06 {
    public static void main(String[] args) {
        int[] integers = new int[26];
        String userString;

        Scanner sc = new Scanner(System.in);

        userString = sc.nextLine();

        //a = 97 in ASCII, z = 122 in ASCII
        for(int i = 0; i < userString.length(); i++){
            char c = userString.charAt(i);
            //convert to lower case (ex. A = 65 in ASCII, this won't add to the array).
            int value = Character.toLowerCase(c);
            if(value >= 97 && value <= 122){
                //0 in array = a (ex. a = 97 in ASCII --> 97 - 97 = 0 --> put it in slot 0 in array)
                int insertArray = value - 97;
                integers[insertArray] += 1;
            }
        }
        for(int i = 0; i < 26; i++){
            //Convert counter to ASCII (ex. 0 + 97 = 97 --> a)
            int intChar = 97 + i;
            char c = (char) intChar;
            System.out.print(c + " --> " + integers[i] + " time(s)  ");
            //Check if there are 4 lines printed already (if i = 4 then it has been printed 4 times).
            //The remainder is 0 then.
            if((i+1) % 4 == 0){
                System.out.println();
            }
        }
    }
}
